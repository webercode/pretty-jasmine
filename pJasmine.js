#!/usr/bin/env node

const path = require('path');
const fs = require('fs');
const ArgumentParser = require('argparse').ArgumentParser;

const appPath = path.resolve(__dirname).split('/node_modules')[0];
const pJasmineConfigPath = path.join(appPath, 'pJasmine.json');

const parser = new ArgumentParser({
	version: require(path.join(__dirname, 'package.json')).version,
	addHelp: true,
	description: ''
});
parser.addArgument(['--init'], {
	help: 'write out a default pJasmine.json',
	action: 'storeTrue',
});
parser.addArgument(['file'], {
	help: 'specific file(s) to pass to jasmine',
	nargs: '*',
	defaultValue: [],
});
const args = parser.parseArgs();

if (args.init)
{
	const defaultConfig = {
		jasmine: {
			spec_dir: 'spec',
			spec_files: ['**/*[sS]pec.js'],
			helpers: ['helpers/**/*.js'],
			random: false,
			seed: null,
			stopSpecOnExpectationFailure: false
		},
		reporter: {
			colors: 1,           // (0|false)|(1|true)|2
			cleanStack: 1,       // (0|false)|(1|true)|2|3
			verbosity: 4,        // (0|false)|1|2|(3|true)|4
			listStyle: 'indent', // "flat"|"indent"
			activity: false
		},
	};

	fs.writeFileSync(pJasmineConfigPath, JSON.stringify(defaultConfig, null, 2));
	console.log('config written to', pJasmineConfigPath);
	process.exit();
}

try
{
	const pJasmineConfig = require(pJasmineConfigPath);

	// setup Jasmine
	const Jasmine = require('jasmine');
	const jasmine = new Jasmine();
	jasmine.loadConfig(pJasmineConfig.jasmine);

	// setup console reporter
	const JasmineConsoleReporter = require('jasmine-console-reporter');
	const reporter = new JasmineConsoleReporter(pJasmineConfig.reporter);

	// initialize and execute
	jasmine.env.clearReporters();
	jasmine.addReporter(reporter);
	jasmine.execute(args.file);
}
catch (e)
{
	console.log('use pJasmine.js --init to initialize pJasmine.json');
	process.exit(1);
}
